---
date: 2017-02-15
title: Food Production & Greenhouse Gases
description: The Paris Agreement, negotiated at the 2015 meeting of the Conference of the Parties of the United Nations Framework Convention on Climate Change (UNFCCC) climate change conference in Paris, included the goal of limiting global climate change to below 2° Celsius.
type: Docs
---
Many people don’t use Jekyll for client projects as non-developers would traditionally have to learn HTML, Markdown and Liquid to update content. In this tutorial, we give non-developers an easy way to update Jekyll sites with [CloudCannon](https://cloudcannon.com).

## What is CloudCannon?

CloudCannon is cloud content management system and hosting provider for Jekyll websites. A developer uploads a Jekyll site in the browser or by syncing with GitHub, Bitbucket or Dropbox. CloudCannon then builds the site, hosts it and provides an interface for non-technical users to update content.

To begin, we need to create a CloudCannon account and create our first site. Head over to [CloudCannon](https://cloudcannon.com) and click the *Get Started Free* button:

Enter your details into the sign up form:

We can view the live site by clicking on the _.cloudvent.net_ URL in the sidebar:

## Editables

Next, we need to do is to define areas in our HTML which non-developers can update. These are called [Editable Regions](https://docs.cloudcannon.com/editing/editable-regions/) and are set by adding a class of `editable` to HTML elements.
