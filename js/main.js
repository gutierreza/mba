var actions = {
  changeclass: function(event) {
		$(".bar-units .link-active").removeClass('link-active');
		$(this).addClass('link-active');
	},
	showhide: function(event) {
		$(this).find('.material-icons').text('keyboard_arrow_down');
		$(this).parent().next().toggleClass('is-visible');
	}
};

function clickAction() {
	$("a[data-action]").on("click", function (event) {
	  var link = $(this),
	      action = link.data("action");

	  event.preventDefault();

	  // If there's an action with the given name, call it
	  if( typeof actions[action] === "function" ) {
	    actions[action].call(this, event);
	  }
	});
}

function showSpeciesFilter() {
	$(".container-svg").click(function() {
		$('.tooltip').hide();
		$('.filter-categories').fadeIn(200);
	});

	$(".btn-close").click(function() {
		$('.tooltip').show();
		$('.filter-categories').fadeOut(100);

		return false;
	});
}

function getSpeciesBarChart() {

  // Get data file
  var speciesData = '/data/species.json';

	$.getJSON(speciesData, function(json) {

		$(json).each(function(index, el) {

				var html = '<div class="species">'
					+ '<div class="col">';

					for (var i = 0; i < el.raised.length; i++) {
						// console.log(el.raised[i].lrange);
						html += '<div class="bar ' + el.classcolor + '" style="height: ' + el.raised[i].lrange + '%; bottom:' + el.raised[i].hrange + 'px">'
						+ '<span class="raised-cat f-size-xxs f-weight-l">' + el.raised[i].type + '</span>'
						+ '<div class="tooltip f-size-s">' + '<h5 class="allcaps f-weight-b">' + el.category + '</h5>' +  'RANGE: ' + el.raised[i].lrange + '-' + el.raised[i].hrange + '</div>'
						+ '</div>'
					}
				html += '</div>'
					+ '<div class="allcaps label f-size-xxs f-weight-l">'+ el.category +'</div>'
					+ '</div>';

			$('.x-axis').append(html);

			var legendhtml = '<li class="barchart-legend-item barchart-legend-item-' + el.classcolor + '">'+ el.category +'</li>'
			$('.barchart-legend').prepend(legendhtml);
		});
	});
}

function getSpecies() {
	var elements = $('.modal-overlay, .modal');

	$('body').delegate('.bar', 'click', function() {
		elements.addClass('active');

		$('body').addClass('overflow-hidden');

		$.scrollify.disable();
	});

	$('body').delegate('.modal-close', 'click', function() {
		elements.removeClass('active');

		$('body').removeClass('overflow-hidden');

		$.scrollify.enable();
	});
}

function getScrollify() {
	$.scrollify({
		section : ".pane",
		setHeights: false,
		scrollSpeed: 1100,
		before:function(index) {
			if (index != 2) {
				$('.box-view-outter').addClass('hide');
			}
		},
		after:function(index) {
			if (index === 2) {
				$('.box-view-outter').removeClass('hide');
			}
		}
	});

	$(".scroll").click(function(e) {
		e.preventDefault();
		$.scrollify.next();
	});
}

function getView() {
	$('.box-view a:not(:first)').addClass('link-active');
	$('.box-view-container').hide();
	$('.box-view-container:first').show();

	$('.box-view a').click(function(){
    var t = $(this).attr('id');

	  if($(this).hasClass('link-active')) {
	    $('.box-view a').addClass('link-active');
	    $(this).removeClass('link-active');

	    $('.box-view-container').hide();
	    $('#'+ t + 'c').fadeIn();
	 }
	});
}

$( document ).ready( function() {

	clickAction();

	getSpeciesBarChart();

	getSpecies();

	getScrollify();

	getView();

	// navigation tabbed
	$(".tab > label").click(function() {
		var forlabel = $(this).attr("for");

		$('nav').removeClass("tab-1 tab-2 tab-3 tab-4 tab-5 tab-6");
		$('nav').addClass(forlabel);
	});
});

// window.onload = function() {
// }
